#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <utmpx.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <limits.h>

void printUserGroups(struct passwd* passwd)
{
	int ngroups = NGROUPS_MAX;
    gid_t* groups = malloc(ngroups * sizeof(gid_t));
    
    if (getgrouplist(passwd->pw_name, passwd->pw_gid, groups, &ngroups) == -1)
    {
        fprintf(stderr, "Error: ngroups = %d\n", ngroups);
        exit(EXIT_FAILURE);
    }

    printf("[ ");
    for (int i = 0; i < ngroups; i++)
    {
        struct group* group = getgrgid(groups[i]);
        printf("%s ", group->gr_name);
    }
    printf("]");
}

void printLoggedUsers(int checkId, int checkGroup)
{
    setutxent();
	struct utmpx* utmp = getutxent();
	while (utmp != NULL)
	{
		if (utmp->ut_type == USER_PROCESS)
		{
			struct passwd* passwd = getpwnam(utmp->ut_user);

			if (checkId == 1)
				printf("%d ", passwd->pw_uid);

			printf("%s ", passwd->pw_name);

			if (checkGroup == 1)
				printUserGroups(passwd);

			printf("\n");
		}

		utmp = getutxent();
	}
	endutxent();
}

int main(int argc, char** argv)
{
	int checkId = 0;
	int checkGroup = 0;

	int option;
	while ((option = getopt(argc, argv, "ig")) != -1)
	{
		switch(option)
		{
			case 'i':
				checkId = 1;
				break;
			case 'g':
				checkGroup = 1;
				break;
			case '?':
                fprintf(stderr, "Usage: [-g] [-i]\n");
			default:
				exit(EXIT_FAILURE);
		}
	}
	
	printLoggedUsers(checkId, checkGroup);

	exit(EXIT_SUCCESS);
}
